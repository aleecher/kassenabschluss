package com.ena.kassenabschluss.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ena.kassenabschluss.R;

import java.util.ArrayList;

/**
 * Adapter für die Kassennamen in der ListView
 */
public class KassenAdapter extends ArrayAdapter<String> {
    // View lookup cache
    private static class ViewHolder {
        TextView name;
    }

    public KassenAdapter(Context context, ArrayList<String> namen) {
        super(context, R.layout.item_kassenauswahl, namen);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        String name = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_kassenauswahl, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.itemAbschlussKassenname);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.name.setText(name);
        // Return the completed view to render on screen
        return convertView;
    }
}