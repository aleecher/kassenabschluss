package com.ena.kassenabschluss.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ena.kassenabschluss.R;

import java.util.ArrayList;

/**
 * Adapter für die Abschlüsse in der Historie
 */
public class AbschlussAdapter extends ArrayAdapter<Abschluss> {
    // View lookup cache
    private static class ViewHolder {
        TextView datum;
        TextView name;
    }

    public AbschlussAdapter(Context context, ArrayList<Abschluss> abschluesse) {
        super(context, R.layout.item_historie, abschluesse);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Abschluss abschluss = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_historie, parent, false);
            viewHolder.datum = (TextView) convertView.findViewById(R.id.itemHistorieDatum);
            viewHolder.name = (TextView) convertView.findViewById(R.id.itemHistorieKassenname);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.datum.setText("Datum: " + abschluss.getDatum());
        viewHolder.name.setText("Kasse: " + abschluss.getKasse());
        // Return the completed view to render on screen
        return convertView;
    }
}