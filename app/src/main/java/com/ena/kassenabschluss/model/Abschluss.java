package com.ena.kassenabschluss.model;

/**
 * Abschluss Klasse
 * Abschluss repräsentiert einen Kassenabschluss
 */
public class Abschluss {

    /*
    * constructor
    */
    public Abschluss(String Name, String Datum, int Cent1, int Cent2, int Cent5, int Cent10
            , int Cent20, int Cent50, int Euro1, int Euro2, int Euro5, int Euro10, int Euro20
            , int Euro50, int Euro100, int Euro200, int Euro500, int Gesamt, String Kassenname)
    {
        this.name = Name;
        this.datum = Datum;
        this.cent1 = Cent1;
        this.cent2 = Cent2;
        this.cent5 = Cent5;
        this.cent10 = Cent10;
        this.cent20 = Cent20;
        this.cent50 = Cent50;
        this.euro1 = Euro1;
        this.euro2 = Euro2;
        this.euro5 = Euro5;
        this.euro10 = Euro10;
        this.euro20 = Euro20;
        this.euro50 = Euro50;
        this.euro100 = Euro100;
        this.euro200 = Euro200;
        this.euro500 = Euro500;
        this.gesamt = Gesamt;
        this.kasse = Kassenname;
    }

    /*
    * to string method containing: name, date and overall sum
    */
    public String toString(){
        String output = this.getName() + "; " + this.getDatum() + "; " + this.getGesamt();
        return output;
    }

    public String getName() {
        return name;
    }

    public String getDatum() {
        return datum;
    }

    public int getCent1() {
        return cent1;
    }

    public int getCent2() {
        return cent2;
    }

    public int getCent5() {
        return cent5;
    }

    public int getCent10() {
        return cent10;
    }

    public int getCent20() {
        return cent20;
    }

    public int getCent50() {
        return cent50;
    }

    public int getEuro1() {
        return euro1;
    }

    public int getEuro2() {
        return euro2;
    }

    public int getEuro5() {
        return euro5;
    }

    public int getEuro10() {
        return euro10;
    }

    public int getEuro20() {
        return euro20;
    }

    public int getEuro50() {
        return euro50;
    }

    public int getEuro100() {
        return euro100;
    }

    public int getEuro200() {
        return euro200;
    }

    public int getEuro500() {
        return euro500;
    }

    public int getGesamt() {
        return gesamt;
    }

    public String getKasse() {
        return kasse;
    }

    /*
    * member variables
    */
    private String name;
    private String datum;
    private int cent1;
    private int cent2;
    private int cent5;
    private int cent10;
    private int cent20;
    private int cent50;
    private int euro1;
    private int euro2;
    private int euro5;
    private int euro10;
    private int euro20;
    private int euro50;
    private int euro100;
    private int euro200;
    private int euro500;
    private int gesamt;
    private String kasse;
}
