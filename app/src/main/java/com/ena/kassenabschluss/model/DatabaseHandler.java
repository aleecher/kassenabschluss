package com.ena.kassenabschluss.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * DatabaseHandler class for accesing database and reading/writing db from/to file
 */
public class DatabaseHandler extends SQLiteOpenHelper{

    /*
    * constructor
    */
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating table for "Abschlüsse"
        String createTable = "CREATE TABLE " + TABLE_ABSCHLUESSE + " ( "
                + "id" + " INTEGER PRIMARY KEY, "
                + "name" + " TEXT, "
                + "datum" + " TEXT, "
                + "cent1" + " INTEGER, "
                + "cent2" + " INTEGER, "
                + "cent5" + " INTEGER, "
                + "cent10" + " INTEGER, "
                + "cent20" + " INTEGER, "
                + "cent50" + " INTEGER, "
                + "euro1" + " INTEGER, "
                + "euro2" + " INTEGER, "
                + "euro5" + " INTEGER, "
                + "euro10" + " INTEGER, "
                + "euro20" + " INTEGER, "
                + "euro50" + " INTEGER, "
                + "euro100" + " INTEGER, "
                + "euro200" + " INTEGER, "
                + "euro500" + " INTEGER, "
                + "gesamt" + " INTEGER, "
                + "kasse" + " TEXT )";

        db.execSQL(createTable);

        // creating table for "Kassen"
        createTable = "CREATE TABLE " + TABLE_KASSEN + " ( "
                + "id" + " INTEGER PRIMARY KEY, "
                + "name" + " TEXT )";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ABSCHLUESSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_KASSEN);

        // Create tables again
        onCreate(db);
    }

    /*
    * insert new "Abschluss" into database
    */
    public void insertAbschluss(Abschluss abschluss) {
        // opening databse connection
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // getting values to put into database
        values.put("name", abschluss.getName());
        values.put("datum", abschluss.getDatum());
        values.put("cent1", abschluss.getCent1());
        values.put("cent2", abschluss.getCent2());
        values.put("cent5", abschluss.getCent5());
        values.put("cent10", abschluss.getCent10());
        values.put("cent20", abschluss.getCent20());
        values.put("cent50", abschluss.getCent50());
        values.put("euro1", abschluss.getEuro1());
        values.put("euro2", abschluss.getEuro2());
        values.put("euro5", abschluss.getEuro5());
        values.put("euro10", abschluss.getEuro10());
        values.put("euro20", abschluss.getEuro20());
        values.put("euro50", abschluss.getEuro50());
        values.put("euro100", abschluss.getEuro100());
        values.put("euro200", abschluss.getEuro200());
        values.put("euro500", abschluss.getEuro500());
        values.put("gesamt", abschluss.getGesamt());
        values.put("kasse", abschluss.getKasse());
        db.insert(TABLE_ABSCHLUESSE, null, values);

        // close database connection
        db.close();
    }

    /*
     * returns an ArrayList of "abschluesse"
     */
    public ArrayList<Abschluss> getAbschluesse() {
        SQLiteDatabase db = this.getReadableDatabase();

        // list for storing the contacts
        ArrayList<Abschluss> abschluesse = new ArrayList<>();

        // getting the contact information from the DB
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ABSCHLUESSE + " GROUP BY id", null);

        // iterating through the rows and saving them to "Abschlüsse" list
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // create new Abschluss from table row
            Abschluss abschluss = new Abschluss(cursor.getString(1), cursor.getString(2),
                    Integer.valueOf(cursor.getString(3)), Integer.valueOf(cursor.getString(4)), Integer.valueOf(cursor.getString(5)),
                    Integer.valueOf(cursor.getString(6)), Integer.valueOf(cursor.getString(7)), Integer.valueOf(cursor.getString(8)),
                    Integer.valueOf(cursor.getString(9)), Integer.valueOf(cursor.getString(10)), Integer.valueOf(cursor.getString(11)),
                    Integer.valueOf(cursor.getString(12)), Integer.valueOf(cursor.getString(13)), Integer.valueOf(cursor.getString(14)),
                    Integer.valueOf(cursor.getString(15)), Integer.valueOf(cursor.getString(16)), Integer.valueOf(cursor.getString(17)),
                    Integer.valueOf(cursor.getString(18)), cursor.getString(19));

            // add Abschluss to list
            abschluesse.add(abschluss);
            cursor.moveToNext();
        }

        // close db connection
        cursor.close();
        db.close();

        // return "abschluesse"-list
        return abschluesse;
    }

    /*
     * returns an "Abschluss" if given a date
     */
    public Abschluss getAbschluss(String datum) {
        SQLiteDatabase db = this.getReadableDatabase();

        // list for storing the contacts
        Abschluss abschluss;

        // getting the contact information from the DB
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ABSCHLUESSE, null);

        // iterating through the rows and saving them to "Abschlüsse" list
        cursor.moveToFirst();
        // create new Abschluss from table row
        abschluss = new Abschluss(cursor.getString(1), cursor.getString(2),
                Integer.valueOf(cursor.getString(3)), Integer.valueOf(cursor.getString(4)), Integer.valueOf(cursor.getString(5)),
                Integer.valueOf(cursor.getString(6)), Integer.valueOf(cursor.getString(7)), Integer.valueOf(cursor.getString(8)),
                Integer.valueOf(cursor.getString(9)), Integer.valueOf(cursor.getString(10)), Integer.valueOf(cursor.getString(11)),
                Integer.valueOf(cursor.getString(12)), Integer.valueOf(cursor.getString(13)), Integer.valueOf(cursor.getString(14)),
                Integer.valueOf(cursor.getString(15)), Integer.valueOf(cursor.getString(16)), Integer.valueOf(cursor.getString(17)),
                Integer.valueOf(cursor.getString(18)), cursor.getString(19));

        // close db connection
        cursor.close();
        db.close();

        // return "abschluesse"-list
        return abschluss;
    }

    /*
    * insert new "Kasse" into database
    */
    public void insertKasse(String name) {
        // opening databse connection
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // getting values to put into database
        values.put("name", name);
        db.insert(TABLE_KASSEN, null, values);

        // close database connection
        db.close();
    }

    /*
    * returns a list with all "Kassen-Ids"
    */
    public ArrayList<String> getKassen() {
        SQLiteDatabase db = this.getReadableDatabase();

        // list for storing the contacts
        ArrayList<String> kassen = new ArrayList<>();

        // getting the contact information from the DB
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_KASSEN + " GROUP BY id", null);

        // iterating through the rows and saving them to "KassenId" list
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // add Id to list
            kassen.add(cursor.getString(1));
            cursor.moveToNext();
        }

        // close db connection
        cursor.close();
        db.close();

        // return "Kassen-Ids"
        return kassen;
    }

    /*
    * returns the "Kassenname" if given an Id
    * returns -1 if nothing found
    */
    public int getKassenId(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        int id = -1;

        // getting the contact information from the DB
        Cursor cursor = db.rawQuery("SELECT id FROM " + TABLE_KASSEN + " WHERE name = '" + name + "'", null);

        // iterating through the rows and saving them to "KassenId" list
        if (cursor.moveToFirst()) {
            id = Integer.valueOf(cursor.getString(0));
        }

        // close db connection
        cursor.close();
        db.close();

        // return "Kassenname"
        return id;
    }

    /*
    * member variables
    */
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "abschlussDB";

    // table name
    private static final String TABLE_ABSCHLUESSE = "abschluesse";
    private static final String TABLE_KASSEN = "kassen";
}
