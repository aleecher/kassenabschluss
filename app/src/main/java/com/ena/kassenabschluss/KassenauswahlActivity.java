package com.ena.kassenabschluss;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.ena.kassenabschluss.model.DatabaseHandler;
import com.ena.kassenabschluss.model.KassenAdapter;

import java.util.ArrayList;

public class KassenauswahlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kassenauswahl);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addKasse();

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        refreshKassenauswahl();
    }

    private void addKasse() {
        // Dialog anlegen um Kassenname entgegen zu nehmen
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Neue Kasse anlegen");
        builder.setMessage("bitte Namen eingeben:");

        // input Variable
        final EditText input = new EditText(this);

        // input-Typ setzen -> hier Text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // nun die Buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Kassenname = input.getText().toString();
                writeKasseToDb();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // zeige den Dialog
        builder.show();
    }

    private void writeKasseToDb() {
        // wenn wir eine gültige Eingabe haben (mind. 1 Zeichen)
        if (!Kassenname.isEmpty()) {
            if (Kassenname.length() > 12) {
                Toast.makeText(this, "Name zu lang (max. 12 Zeichen)", Toast.LENGTH_SHORT);
            } else {
                // Datenbank Verbindung
                DatabaseHandler db = new DatabaseHandler(this);

                // -1 not existing; else returned id
                boolean exists = false;
                // Kasse in Datenbank eintragen
                try {
                    if (db.getKassenId(Kassenname) == -1) {
                        db.insertKasse(Kassenname);
                    } else {
                        exists = true;
                    }
                } catch (Exception e) {
                    Toast.makeText(this, "Fehler beim Erstellen der Kasse", Toast.LENGTH_SHORT).show();
                }
                if (exists) {
                    Toast.makeText(this, "Name existiert bereits", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Kasse angelegt", Toast.LENGTH_SHORT).show();
                }
                // close database connection
                db.close();
            }
        } else {
            Toast.makeText(this, "Ungültiger Name", Toast.LENGTH_SHORT).show();
        }

        Kassenname = "";
        refreshKassenauswahl();

    }

    /*
    * aktualisiert die ListView
    */
    private void refreshKassenauswahl() {
        // handle zur ListView
        ListView lv = (ListView) findViewById(R.id.listView_Kassen);
        // ArrayList für unsere Kassennamen
        kassen = new ArrayList<>();

        // Datenbank Verbindung
        DatabaseHandler db = new DatabaseHandler(this);
        try {
             kassen = db.getKassen();
        } catch (Exception e) {
            // clean install - keine Abschlüsse vorhanden
        }
        // Datenbankverbindung schließen
        db.close();

        if (!kassen.isEmpty()) {
            KassenAdapter kassenAdapter = new KassenAdapter(this, kassen);
            lv.setAdapter(kassenAdapter);

            // onClickListener setzen
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                    String kasse = (String) parent.getItemAtPosition(position);
                    openAbschlussAnlegen(kasse);
                }
            });
        }
    }

    /*
    * öffnet die Kassenabschluss erstellen activity
    */
    private void openAbschlussAnlegen(String kasse) {
        Intent i = new Intent(this, AbschlussAnlegenActivity.class);
        i.putExtra("Kassenname", kasse);
        startActivity(i);
    }

    // member variables
    private String Kassenname = "";
    private ArrayList<String> kassen;
}
