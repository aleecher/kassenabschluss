package com.ena.kassenabschluss;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ena.kassenabschluss.model.Abschluss;
import com.ena.kassenabschluss.model.AbschlussAdapter;
import com.ena.kassenabschluss.model.DatabaseHandler;
import com.ena.kassenabschluss.model.KassenAdapter;

import java.util.ArrayList;

public class HistorieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historie);

        refreshHistorie();
    }

    /*
    * aktualisiert die ListView
    */
    private void refreshHistorie() {
        // handle zur ListView
        ListView lv = (ListView) findViewById(R.id.listView_Historie);
        // ArrayList für unsere Kassennamen
        abschluesse = new ArrayList<>();

        // Datenbank Verbindung
        DatabaseHandler db = new DatabaseHandler(this);
        try {
            abschluesse = db.getAbschluesse();
        } catch (Exception e) {
            // clean install - keine Abschlüsse vorhanden
        }
        // Datenbankverbindung schließen
        db.close();

        if (!abschluesse.isEmpty()) {
            AbschlussAdapter abschlussAdapter = new AbschlussAdapter(this, abschluesse);
            lv.setAdapter(abschlussAdapter);

            // onClickListener setzen
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                    Abschluss abschluss = (Abschluss) parent.getItemAtPosition(position);
                    openAbschlussDetails(abschluss.getDatum());
                }
            });
        }
    }

    /*
    * öffnet die Detailansicht zum gewählten Abschluss
    */
    private void openAbschlussDetails(String abschlussDatum) {
        Intent i = new Intent(this, AbschlussUebersichtActivity.class);
        // Übergabe des ausgewählten Abschlusses
        i.putExtra("datum", abschlussDatum);
        startActivity(i);
    }

    // member variables
    private ArrayList<Abschluss> abschluesse;
}
