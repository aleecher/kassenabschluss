package com.ena.kassenabschluss;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ena.kassenabschluss.model.Abschluss;
import com.ena.kassenabschluss.model.DatabaseHandler;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Abschlüsse aus der Datenbank holen
        ArrayList<Abschluss> abschluesse = new ArrayList<>();
        Abschluss abschluss = null;

        // unsere Datenbankverbindung wird initialisiert
        DatabaseHandler db = new DatabaseHandler(this);

        // DEBUG
        Abschluss debug = new Abschluss("test3", "13.06.2016", 1, 2, 5, 10, 20, 50, 11, 12, 15, 110, 120, 150, 1100, 1200, 1500, 8888, "alpha");
        db.insertAbschluss(debug);
        debug = new Abschluss("test4", "14.06.2016", 1, 2, 5, 10, 20, 50, 11, 12, 15, 110, 120, 150, 1100, 1200, 1500, 2222, "beta");
        db.insertAbschluss(debug);
        db.insertKasse("Kasse_A");
        db.insertKasse("Kasse_B");

        try {
            abschluesse = db.getAbschluesse();
            // den zuletzt erstellten raussuchen
            abschluss = abschluesse.get(abschluesse.size() - 1);
        } catch (Exception e) {
            // clean install - keine Abschlüsse vorhanden
        }

        // setzen der TextFelder im Hauptmenü
        // Anzahl der Kassen
        TextView tv = (TextView) findViewById(R.id.txtKassenanzahl);
        if (abschluesse.isEmpty()) {
            tv.setText("0");
        } else {
            tv.setText("" + abschluesse.size());
        }

        // Gesamtsumme aller Kassenabschlüsse
        tv = (TextView) findViewById(R.id.txtGesamtsumme);
        if (abschluesse.isEmpty()) {
            tv.setText("0");
        } else {
            int sum = 0;
            for (int i = 0; i < abschluesse.size(); i++) {
                sum += abschluesse.get(i).getGesamt();
            }

            // Summe intern in Cent, daher Umrechnung in Euro
            double output = (double) sum / 100;
            tv.setText(output + " €");
        }

        // letzter Kassenabschluss: Kaasenname
        tv = (TextView) findViewById(R.id.txtAbschlussKasse);
        if (abschluesse.isEmpty()) {
            tv.setText("");
        } else {
            tv.setText(abschluss.getKasse());
        }

        // letzter Kassenabschluss: Kaasenname
        tv = (TextView) findViewById(R.id.txtAbschlussDatum);
        if (abschluesse.isEmpty()) {
            tv.setText("");
        } else {
            tv.setText(abschluss.getDatum());
        }
    }

    /*
     * öffnet den Kassenauswahl-Screen
     */
    public void openKassenauswahl(View view) {
        Intent i = new Intent(this, KassenauswahlActivity.class);
        startActivity(i);
    }

    /*
     * öffnet die Historie (Übersicht aller vergangenen Abschlüsse)
     */
    public void openHistorie(View view) {
        Intent i = new Intent(this, HistorieActivity.class);
        startActivity(i);
    }
}
