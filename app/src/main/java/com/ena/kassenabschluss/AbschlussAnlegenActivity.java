package com.ena.kassenabschluss;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class AbschlussAnlegenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abschluss_anlegen);

        // trage Kassennamen ein
        this.kassanname = getIntent().getStringExtra("Kassenname");

        // TODO: Implementierung -> view-Inhalte entgegen nehmen und Abschluss, wenn gültig, anlegen
    }

    // member variables
    String kassanname = "";
}
