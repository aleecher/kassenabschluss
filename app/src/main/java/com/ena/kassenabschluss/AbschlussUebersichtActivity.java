package com.ena.kassenabschluss;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.ena.kassenabschluss.model.Abschluss;
import com.ena.kassenabschluss.model.DatabaseHandler;

public class AbschlussUebersichtActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abschluss_uebersicht);

        // Datenbank Verbindung um ausgewählten Abschluss zu laden
        DatabaseHandler db = new DatabaseHandler(this);

        try {
            this.abschluss = db.getAbschluss(getIntent().getStringExtra("abschlussDatum"));
        } catch (Exception e) {
            Toast.makeText(this, "Fehler beim laden des Abschlusses", Toast.LENGTH_SHORT).show();
        }
        // Verbindung zur Datenbank schließen
        db.close();

        if (this.abschluss != null) {
            // TODO: Implementierung -> view mit Daten füllen
        }
    }

    // member variables
    private Abschluss abschluss;
}
